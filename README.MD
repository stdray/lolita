### Lolita Open Source Edition v0.0.1

Difference between Enterprise Edition and Open Source Edition

1. Markov Chain run with the same engine (in EE with thrift server)
2. A smaller number of plugins
3. Non stable (while)


### How to use

1. install java
2. install jruby (jruby-9.0.3.0)
3. run `bundle`
4. run `rake check`
5. run `rake chat`
6. run `ruby lolita.rb`
7. enjoy



License: MIT



