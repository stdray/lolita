require 'yaml'
require 'active_support/all'

class Configuration
  def initialize
    file = "#{Dir.pwd}/config/config.yml"
    yaml = YAML.load_file(file)
    yaml.each do |line|
      k, v = line
      define_singleton_method k.underscore.to_sym do
        v
      end
    end
  end
end
