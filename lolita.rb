require "java"
require "logger"
global = File.dirname(__FILE__)
core = "core"
vendor = "vendor"
[vendor, core].each {|d| $:.unshift "#{global}/#{d}" }
require "#{global}/#{core}/configuration"
require "#{File.dirname(__FILE__)}/core/bot"

room = "ROOM@conference.jabber.ru"
server = "SERVER"
user = "pass"
pass = "pass"

$logger = Logger.new("#{global}/logger.log")

conf = Configuration.new

bot = Bot.new(server, user, pass, conf)
bot.join(room)
#
loop do
  sleep 1000
end

