#encoding: utf-8
require 'nokogiri'
require 'rest-client'

module Lor
  extend self
  URL = "http://www.linux.org.ru"
  UA =  'Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0'
  def get
    req = RestClient.get(URL, "User-Agent" => UA)
    doc = Nokogiri::HTML(req.body)
    doc.css("article").slice(0, 6).map do |article|
      link = article.css('a:first').first
      "\n #{link.text} #{URL}#{link.get_attribute('href')} "
    end.join
  end
end
