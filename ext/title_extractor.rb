require 'rest-client'
require 'nokogiri'
require 'timeout'

module TitleExtractor

  UserAgents = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1',
    'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0',
    'Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0',
    'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.1 Safari/537.36',
    'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2227.0 Safari/537.36',
    'Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16'
  ]

  Time = 7

  class << self
    #
    # raise Errors on extract
    def extract(url)
      Timeout::timeout(self::Time) do
        request_user_agent = self::UserAgents.sample
        request = RestClient.get(url, "User-Agent" => request_user_agent)
        doc = Nokogiri::HTML(request.body)
        title = doc.css("title")
        if !title.nil? && title.size > 0
          title.first.text
        else
          raise RuntimeError.new("fail when extract title for #{url}")
        end
      end
    end
  end
end


