require_relative '../configuration'
module ConfigHelper
  def self.included(base)
    # FIXME use system config
    config = Configuration.new
    [:help, :nicks].each do |cm|
      base.send(:define_method, cm) do
        config.send(cm)
      end
    end
  end
end