#encoding:utf-8
require 'nokogiri'
require 'rest-client'

module Opennet
  extend self
  URL = 'http://opennet.ru'
  UA =  'Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0'
  def get
    req = RestClient.get(URL, "User-Agent" => UA)
    doc = Nokogiri::HTML(req.body)
    z = doc.css('table.tlist')
    right = z[0].css('tr td a').slice(0, 3).map do |link|
      "#{link.text} - #{URL+link.get_attribute('href')}"
    end
    left = z[1].css('tr td a').slice(0, 3).map do |link|
      "#{link.text} - #{URL+link.get_attribute('href')}"
    end
    (right + left).join("\n")
  end
end