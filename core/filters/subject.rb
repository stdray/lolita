require_relative '../message'

class SubjectListener

  include org.jivesoftware.smackx.muc.SubjectUpdatedListener

  def initialize(message_handler)
    @message_handler = message_handler
  end

  def subjectUpdated(subject, from)
    from = from.split('/').last
    info = subject.strip.empty? ? "has unset the topic" : "has set the topic to: #{subject}"
    @message_handler << Message.system("#{from} #{info}")
  end
end
