require_relative '../message'

class PacketListener
  include org.jivesoftware.smack.PacketListener

  def initialize(bot, message_handler)
    @connection = bot.connection
    @message_handler = message_handler
  end

  def process(packet)
    from = packet.from || "~"
    chat = @connection.get_chat_manager.create_chat(from, MessageListener.new)
    @message_handler << Message.local(packet.body, chat)
  end

  alias :processPacket :process
end
