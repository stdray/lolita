require 'java'
require 'configuration'

Dir["#{Dir.pwd}/vendor/*"].each {|jar|
  require jar
}
Dir["#{Dir.pwd}/core/filters/*.rb"].each {|file| require file}

require_relative "instrument/plugin_compiler"
require_relative "message_hadnler"

class Bot

  Smack  = org.jivesoftware.smack
  Smackx = org.jivesoftware.smackx

  attr_accessor :joined, :connection, :name, :password, :muc

  def port
    5222
  end

  def initialize(server, name, password, configuration)
    @server, @name, @password, @config = server, name, password, configuration
    @joined = false
    @connection = nil
    PluginCompiler::compile(@config)
  end

  def join(room)
    conf = Smack.ConnectionConfiguration.new(@server, port)
    conf.setReconnectionAllowed(true)

    @connection = Smack.XMPPConnection.new(conf)
    @connection.connect
    @connection.login(@name, @password)

    @muc = Smackx.muc.MultiUserChat.new(@connection, room)
    history = Smackx.muc.DiscussionHistory.new
    history.set_max_stanzas(0)

    nickname = "lolita" #Configuration::Nicks.first || 'lolita'
    @muc.join(nickname, "", history, Smack.SmackConfiguration.getPacketReplyTimeout)
    @joined = true
    initialize_listeners
    $logger.info("Enter in '#{room}' room")
  end


  def join?
    @joined
  end

  def close?
    !join?
  end

  def close
    @connection.disconnect
    @joined = false
  end

  def send(message, chat = nil)
    begin
      unless message.nil?
        if chat.nil?
          @muc.send_message(message)
        else
          chat.send_message(message)
        end
      end
    rescue Exception => e
      $logger.error(e)
      send(e.message, chat)
    end
  end

  private

  def initialize_listeners
    behaviour = Concurrent::Actor::Behaviour.restarting_behaviour_definition {
      lambda { |message| $logger.error("failed with #{message}") }
    }
    mh = MessageHandler.spawn(
        name: :message_handler,
        behaviour_definition: behaviour,
        args: [@muc, @config]
    )
    @muc.add_message_listener(ChatListener.new(mh)) #for listen MUC
    @muc.add_participant_status_listener(PresenceListener.new(mh))
    @muc.add_subject_updated_listener(SubjectListener.new(mh))
    @connection.add_packet_listener(PacketListener.new(self, mh), PacketFilter.new) #for Private Messages
  end

end





