require 'concurrent'

require_relative '../markov-chain-bot-module/auto_marshalling_map'
require_relative '../markov-chain-bot-module/markov_chain'
require_relative '../markov-chain-bot-module/chat_bot_gen'

class MarkovChainBotHelper

  def initialize
    @storage = MarkovChainChatBot.from(Hash.new)
    file = File.read(file_name).each_line.to_a
    file[0..1000].each do |txt|
      line = txt.strip.split(" ")
      if line.size > 3
        msg = line.join(" ")
        if !msg.include?("coding@conference") || !msg.include?("bomban") || !msg.include?("livejournal") || !msg.include?("youtube")
          @storage.learn(msg)
        end
      end
    end
  end


  def call(word)
    @storage.answer(word).gsub("\n", ' ').squeeze(' ').delete("\n")
  end

  private def file_name
    "#{Dir.pwd}/1.txt"
  end

end

