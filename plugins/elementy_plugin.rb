#encoding: utf-8
require 'nokogiri'
require 'rest-client'

module Elementy
  extend self
  UA =  'Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0'
  URL = "http://elementy.ru/"
  SELECTOR = "td.science_news td.science_news span > a"
  def get
    req = RestClient.get(URL, "User-Agent" => UA)
    doc = Nokogiri::HTML(req.body)
    doc.css(SELECTOR)[0..5].map do |n|
      text = n.text
      url = n.get_attribute("href")
      "#{text}: #{URL}#{url}"
    end.join("\n")
  end
end
