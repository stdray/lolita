#encoding: utf-8
require 'nokogiri'
require 'rest-client'

module Horo

  class << self

    SIGNS = %w(aries taurus gemini cancer leo virgo
               libra scorpio sagittarius
               capricorn aquarius pisces)

    SIGNS_RU = %w(овен телец близнецы рак лев дева весы
                 скорпион стрелец козерог водолей
                 рыбы
                )

    SIGNS_NUM = %w(4 5 6 7 8 9 10 11 12 1 2 3).map(&:to_i)

    def get_url_for_zodiak(sign)
      sign = sign.downcase
      sign = if SIGNS.include?(sign)
               sign
             elsif SIGNS_RU.include?(sign)
               q = SIGNS_RU.zip(SIGNS)
               z = Hash[*q.flatten]
               z[sign]
             elsif sign.to_i != 0 && SIGNS_NUM.include?(sign.to_i)
               q = SIGNS_NUM.zip(SIGNS)
               z = Hash[*q.flatten]
               z[sign.to_i]
             else
               en = SIGNS.map do |s|
                 levenshtein(s, sign)
               end.sort do |a, b|
                 a.last <=> b.last
               end.first

               ru = SIGNS_RU.map do |s|
                 levenshtein(s, sign)
               end.sort do |a, b|
                 a.last <=> b.last
               end.first

               if en.last < ru.last
                 en.first
               elsif en.last == ru.last
                 SIGNS.sample
               else
                 s = ru.first
                 q = SIGNS_RU.zip(SIGNS)
                 z = Hash[*q.flatten]
                 z[s]
               end
             end

      q = SIGNS.zip(SIGNS_RU)
      z = Hash[*q.flatten]
      znak = z[sign]

      [znak, "https://horo.mail.ru/prediction/#{sign}/today/"]
    end

    def get_for(arr)
      sign = if arr.class == Array
               arr.flatten.first
             else
               arr
             end

      url = get_url_for_zodiak(sign)
      req = RestClient.get(url.last)
      doc = Nokogiri::HTML(req.body)

      text = doc.css("div.article__text").text()
      z = doc.css("div.p-score-day span.p-score-day__item__value__inner")
      b = z[0].text()
      l = z[1].text()
      h = z[2].text()

      %Q{
      #{url.first}
      #{text}
        бизнес: #{b}, любовь: #{l}, здоровье: #{h}
      }
    end

    def levenshtein(first, second)
      matrix = [(0..first.length).to_a]
      (1..second.length).each do |j|
        matrix << [j] + [0] * (first.length)
      end

      (1..second.length).each do |i|
        (1..first.length).each do |j|
          if first[j-1] == second[i-1]
            matrix[i][j] = matrix[i-1][j-1]
          else
            matrix[i][j] = [
                matrix[i-1][j],
                matrix[i][j-1],
                matrix[i-1][j-1],
            ].min + 1
          end
        end
      end
      [first, matrix.last.last]
    end

  end
end
