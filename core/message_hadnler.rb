require 'active_support/core_ext/string'
require 'active_support/core_ext/object'
require 'uri'
require 'rest-client'
require 'concurrent'
require 'concurrent-edge'
require 'timeout'

require_relative '../ext/title_extractor'
require_relative '../ext/markov_chain_bot_helper'
require_relative '../ext/db_writer'
require_relative 'message'

class MessageHandler < Concurrent::Actor::Context
  attr_accessor :muc, :config, :plugins, :help_viewer

  include_package 'client'

  def initialize(muc, config)
    @muc = muc
    @config = config
    @markov_chains = MarkovChainBotHelper.new
    @db = DbWriter.new
    # todo move on top level
    @plugins = import_plugins!
  end

  def on_message(message)
    if Message === message
      begin
        point = message.source || @muc

        if message.system? || message.public?
          @db.call(message.original, message.from)
        end

        if message.command?
          plugin = detect_plugin(message.command)
          args = plugin.nil? ? message.original : message.arguments
          timeoutable(plugin || @markov_chains, args)
          .on_success { |result|
            point.send_message(result.to_s)
          }.on_failure { |fail|
            if plugin.nil?
              $logger.warn("Call #{message} as command failed -> #{fail}")
              point.send_message("Call #{message.command} failed")
            else
              $logger.warn("Call markov chains failed with #{message}")
            end
          }
        else
          # possible url
          if message.url?
            Concurrent.future {
              TitleExtractor.extract(message.url)
            }.on_success { |result|
              point.send_message(result)
            }.on_failure { |fail|
              $logger.warn("Failed extract url for #{message.url} -> #{fail}")
            }
          end
        end
      rescue Exception => ex
        $logger.error(ex.message)
      end
    end
  end

  private def timeoutable(plugin, args)
    Concurrent.future {
      Timeout::timeout(timeout) {
        plugin.call(args)
      }
    }
  end

  private def timeout
    7 # seconds
  end

  private def import_plugins!
    Dir["#{Dir.pwd}/#{config.compile_dir}/*_controller.rb"].map do |file|
      require file
      file.split("/").last.split(".").first.classify.constantize.new
    end
  end

  private def detect_plugin(command)
    @plugins.find {|plugin| plugin.commands.include?(command) }
  end
end




