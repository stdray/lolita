require_relative '../message'

class ChatListener
  include org.jivesoftware.smack.PacketListener

  def initialize(message_handler)
    @message_handler = message_handler
  end

  def process(packet)
    from = packet.from.split('/').last
    @message_handler << Message.create(packet.body, from)
  end
  alias :processPacket :process
end
