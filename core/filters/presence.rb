require_relative '../message'

class PresenceListener
  include org.jivesoftware.smackx.muc.ParticipantStatusListener

  def initialize(message_handler)
    @message_handler = message_handler
  end

  def adminGranted(z)
  end

  def adminRevoked(z)
  end

  def joined(nick)
    @message_handler << Message.system("#{nick}  has joined")
  end

  def membershipGranted(z)
  end

  def membershipRevoked(z)
  end

  def moderatorGranted(z)
  end

  def moderatorRevoked(z)
  end

  def ownershipGranted(z)
  end

  def ownershipRevoked(z)
  end

  def voiceGranted(z)
  end

  def voiceRevoked(z)
  end

  def left(nick)
    if Random.rand(21) == 1
      @message_handler << Message.create("bombanulo~")
    end
    @message_handler << Message.system("#{nick} has left")
  end

  def nicknameChanged(from, to)
    @message_handler << Message.system("#{from} ~> #{to}")
  end
end
