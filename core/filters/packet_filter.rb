class PacketFilter
  include org.jivesoftware.smack.filter.PacketFilter
  def accept!(packet)
    return true if packet.type.to_string == "chat"
    false
  end
  alias :accept :accept!
end