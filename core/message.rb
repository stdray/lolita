require 'uri'
require_relative 'util/message_helper'

class Message
  include ConfigHelper

  private_class_method :new

  attr_accessor :nick, :command
  attr_accessor :arguments, :from
  attr_accessor :source, :is_system
  attr_accessor :is_local, :original

  alias :url :nick

  def initialize(message, from, source, is_local, is_system)
    @original = message
    message = message.strip.split(' ')
    @nick, @command, @arguments = message[0], message[1], message[2..-1]
    @from = from
    @source = source
    @is_local = is_local
    @is_system = is_system
  end

  class << self
    # for muc
    def create(message, from)
      Message.send(:new, message, from, nil, false, false)
    end

    # just events in system (joined, change chat theme, etc...)
    def system(message)
      Message.send(:new, message, nil, nil, false, true)
    end

    # private messages
    def local(message, source)
      Message.send(:new, message, nil, source, true, false)
    end
  end

  def system?
    @is_system
  end

  def local?
    @is_local
  end

  def public?
    !local?
  end

  def command?
    nicks.include?(@nick)
  end

  def url?
    uri = URI.parse(@nick)
    uri.kind_of?(URI::HTTP)
  rescue URI::InvalidURIError
    false
  end

  def to_s
    "#{@original} \n is_system = #{@is_system} \n is_local = #{@is_local}"
  end
end


