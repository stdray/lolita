require 'yaml'
require 'active_support/core_ext/string'
require 'fileutils'
require 'erb'

module PluginCompiler

  class Tmp
    attr_accessor :module_name, :arguments, :plugin_dir
    def initialize(mod, plugin_dir)
      @module_name, @arguments = mod
      @plugin_dir = plugin_dir
    end
    def get_binding
      binding()
    end
  end

  class << self
    def check_uniq(doc)
      all = doc.map { |mod| mod[1]['commands'] }.flatten
      if all.size != all.uniq.size
        raise RuntimeError.new('Duplicate command')
        exit
      end
    end

    def compile(config)
      command_file = "#{Dir.pwd}/#{config.command_file}"
      plugin_dir = "#{Dir.pwd}/#{config.plugin_dir}"
      output = "#{Dir.pwd}/#{config.compile_dir}"
      template = ERB.new File.read("#{__dir__}/plugin_controller.erb")


      doc = YAML.load_file(command_file)

      check_uniq(doc)

      FileUtils::mkdir_p output

      doc.each do |mod|
        tmp = Tmp.new(mod, plugin_dir)
        File.open("#{output}/#{tmp.module_name.underscore}_controller.rb", "w") do |file|
          file.write(template.result(tmp.get_binding))
        end
      end

      plugin_help = doc.map { |mod| %Q{\n[#{mod[1]['commands'].join(', ')}] (#{mod[1]['access']}) ---- #{mod[1]['help']}} }
      system_help = %Q{

        Help: [#{config.help.join(', ')}]
        Logs: #{config.logs}
        Nicks: [#{config.nicks.join(', ')}]
      }.strip_heredoc

      help = Tmp.new(['Help', {
       'commands' => "#{config.help}" ,
       'script' => 'self::help',
       'access' => 'all',
       'args' => '[]',
       'help' => "#{(plugin_help << system_help).join(' ')}"
      }], plugin_dir)
      File.open("#{output}/#{help.module_name.underscore}_controller.rb", "w") do |file|
        file.write(template.result(help.get_binding))
      end

    end
  end
end
